# Text Detection


The concept is simple. Opencv is used to read an image and preform certain image processing, while tesseract is used to recognize Texts from images.

But first check if Tesseract installed or not. The installation steps are different from Mac, Ubuntu or Windows, so check the documentation. 

# Steps

to install Tesseract:

`sudo apt install tesseract-ocr`

**Coding**

1. Read the image you want with _imread_ function

2. Apply image processing for the image

    Here we convert our image into grayscale image, then we apply threshold to the converted image

3. Create a structural element; rectangular structural element. Also Dilate.

4. Find Contours in the dilated image

    All the above image processing techniques are applied so that the Contours can detect the boundary edges of the blocks of text of the image.


5. Apply the OCR
    
    Loop through each contour and take the x and y coordinates and the width and height, then draw a rectangle in the image.

6. Crop the rectangular region and pass it to the tesseract to extract the image

7. print the extracted text


---
The image Sample

![software](/uploads/1c670df64a3c3c80dd644ea2ea834f11/software.jpg)

The results:

![Screenshot_2024-06-27_at_09.33.09](/uploads/8fe635ec670b0778905335528e2c9dd9/Screenshot_2024-06-27_at_09.33.09.png)

--- 

# Easyocr

The second code is using Easyocr for text Detection. It took me a while until tesseract worke so i had to find another way to detect the text.

At the beginning a function is defined. It takes an image and a list of Detection results, then draws bounding boxes around detected objects and draws text on the image. 

The results:

![Screenshot_2024-06-27_at_09.34.06](/uploads/8f2bf49dfb874c515128473253066618/Screenshot_2024-06-27_at_09.34.06.png)

--- 

_Links:_


https://pyimagesearch.com/2018/09/17/opencv-ocr-and-text-recognition-with-tesseract/


https://www.geeksforgeeks.org/text-detection-and-extraction-using-opencv-and-ocr/


https://www.youtube.com/watch?v=qWwQ92ANxVw&list=PLPvRKBf-viXJqNqBpbaYis5FcUVKOZKEk

---
