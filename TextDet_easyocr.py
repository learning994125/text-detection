import cv2
import easyocr
import matplotlib.pyplot as plt

def draw_bounding_boxws(image, detections, threshold=0.25):
    #Draw the bounding box and Put the text near the bounding box
    for bbox, text, score in detections:
        if score > threshold:
            cv2.rectangle(image, tuple(map(int, bbox[0])), tuple(map(int, bbox[2])), (0, 255, 0), 2)
            cv2.putText(image, text, tuple(map(int, bbox[0])), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.65, (255, 0, 0), 2)

#Read img
image_path = "/Users/a_dido97/Desktop/software.jpg" 
img = cv2.imread(image_path)

#Check if the image is loaded
if img is None:
    raise ValueError("Error loading image")

#Initialize EasyOCR reader and Perform text detection
reader = easyocr.Reader(['en'], gpu=True)
text_dete = reader.readtext(img)

# Draw bounding boxes around detected text
threshold = 0.25
draw_bounding_boxws(img, text_dete, threshold)

# Convert BGR image to RGB for displaying with matplotlib
img_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
plt.imshow(img_rgb)
plt.axis('off')
plt.show()

