import cv2
import pytesseract
import os

#Read the image
image_path = '/Users/a_dido97/Desktop/software.jpg'
image = cv2.imread(image_path)
if image is None:
    raise ValueError(f"Failed to load image '{image_path}'.")

#Image to grayscale
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

#Thresholding
ret, thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

#Create a structuring element
rect = cv2.getStructuringElement(cv2.MORPH_RECT, (18, 18))
dilation = cv2.dilate(thresh, rect, iterations=1)

#Find contours and draw contour around detected text and extract it
contours, hierarchy = cv2.findContours(dilation, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

image2 = image.copy()
for cnt in contours:
    x, y, w, h = cv2.boundingRect(cnt)
    rectDraw = cv2.rectangle(image2, (x, y), (x + w, y + h), (0, 255, 0), 2)

    cropped = image2[y:y + h, x:x + w]
    text = pytesseract.image_to_string(cropped)

    print("Text is:", text)

while (True):

    cv2.imshow("Detected Text", image2)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break



